import { Component, Input} from '@angular/core';

@Component({
  selector: 'app-sampletest',
  templateUrl: './sampletest.component.html',
  styleUrls: ['./sampletest.component.css']
})
export class SampletestComponent {
    // @Input() testvalue: number;
    obj= {
      name: 'durga',
      id: 34
    };
    arr= ['chinni', 'durga', 'himani'];
    isTrue = false;
    myname = 'durga';
  }
