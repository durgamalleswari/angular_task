import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TodoComponent } from './todo/todo.component';
import { TodosComponent } from './todos/todos.component';
import { TodoFormComponent } from './todo-form/todo-form.component';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoService } from './todo.service';
import { FormsModule } from '@angular/forms';
import { SampletestComponent } from './sampletest/sampletest.component';
import { SignupFormComponent } from './signup-form/signup-form.component';
import { EqualValidator } from './equalvalidator.directive';
import { Routes, RouterModule } from '@angular/router';
import { HelloComponent } from './hello/hello.component';
export const ROUTES: Routes = [
  { path: 'todos', component: TodosComponent },
  { path: 'signup-form', component: SignupFormComponent },
  {path: 'sampletest', component: SampletestComponent},
  {path: 'hello/:name', component: HelloComponent},


];
@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodosComponent,
    TodoFormComponent,
    TodoListComponent,
    SampletestComponent,
    SignupFormComponent,
    EqualValidator,
    HelloComponent,
  ],
  imports: [
    BrowserModule, FormsModule, RouterModule.forRoot(ROUTES)
  ],
  providers: [TodoService],
  bootstrap: [AppComponent]
})
export class AppModule { }


